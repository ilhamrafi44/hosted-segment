<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HouseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', [HouseController::class, 'index'])->name('/');

Auth::routes(['verify' => true]);

//USER
Route::get('/Users', [HomeController::class, 'index'])->middleware(['user', 'verified'])->name('Users');
Route::get('/transaksi/{event_id}', [HomeController::class, 'transaksi'])->middleware(['user', 'verified'])->name('transaksi');
Route::get('/activities/{event_id}', [HomeController::class, 'activities'])->middleware(['user', 'verified'])->name('activities');
Route::post('/insertAct', [HomeController::class, 'insertAct'])->middleware(['user', 'verified'])->name('insertAct');
Route::put('/userUpdate/{id}', [HomeController::class, 'userUpdate'])->middleware(['user', 'verified'])->name('userUpdate');
Route::put('/updateAct/{id}', [HomeController::class, 'updateAct'])->middleware(['user', 'verified'])->name('updateAct');
Route::put('/updatepassword', [HomeController::class, 'updatePassword'])->middleware(['user', 'verified'])->name('updatepassword');

//ADMIN
Route::get('/admin', [AdminController::class, 'index'])->middleware('admin')->name('/admin');
Route::get('/events', [AdminController::class, 'events'])->middleware(['admin'])->name('events');
Route::get('/memberlist', [AdminController::class, 'memberlist'])->middleware(['admin'])->name('memberlist');
Route::get('/memberaktif', [AdminController::class, 'memberaktif'])->middleware(['admin'])->name('memberaktif');
Route::get('/membernon', [AdminController::class, 'membernon'])->middleware(['admin'])->name('membernon');
Route::put('/confirmpay/{id}', [AdminController::class, 'confirmpay'])->middleware(['admin'])->name('confirmpay');
Route::get('/activitylist', [AdminController::class, 'activitylist'])->middleware(['admin'])->name('activitylist');

Route::post('/insertevents', [AdminController::class, 'insertEvents'])->middleware(['admin'])->name('insertevents');
Route::post('/insertregist', [AdminController::class, 'insertRegist'])->middleware(['admin'])->name('insertregist');
Route::post('/inserttrans', [HomeController::class, 'insertTrans'])->middleware(['user'])->name('inserttrans');
Route::get('/eventdetail/{event_id}', [AdminController::class, 'eventdetail'])->middleware(['admin'])->name('eventdetail');
Route::get('/eventDestroy/{event_id}', [AdminController::class, 'eventDestroy'])->middleware(['admin'])->name('eventDestroy');
Route::put('/insertEditEvent/{id}', [AdminController::class, 'insertEditEvent'])->middleware(['admin'])->name('insertEditEvent');
Route::post('/insertUser', [AdminController::class, 'insertEvents'])->middleware(['admin'])->name('insertUser');
Route::get('/transDestroy/{trans_id}', [AdminController::class, 'transDestroy'])->middleware(['admin'])->name('transDestroy');

Route::get('auth/google', [GoogleController::class, 'redirectToGoogle']);
Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);
Route::get('auth/facebook', [GoogleController::class, 'facebookRedirect']);
Route::get('auth/facebook/callback', [GoogleController::class, 'loginWithFacebook']);
