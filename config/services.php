<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
     */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
        'client_id' => '979542689279-nilkrjq95l7c27kcouipdt12h9h9thka.apps.googleusercontent.com',
        'client_secret' => 'j3VhwUduXSdaiW_HB0iL1itT',
        'redirect' => 'https://segment.events/auth/google/callback',
    ],

    'facebook' => [
        'client_id' => '876473346315821',
        'client_secret' => '1c1b1a5572d11b9a28d902e47966c52a',
        'redirect' => 'https://segment.events/auth/facebook/callback',
    ],

    // 'twilio' => [
    //     'sid' => env('TWILIO_AUTH_SID'),
    //     'token' => env('TWILIO_AUTH_TOKEN'),
    //     'whatsapp_from' => env('TWILIO_WHATSAPP_FROM')
    //   ],

];
