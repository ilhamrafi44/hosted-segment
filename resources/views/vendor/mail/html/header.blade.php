<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Segment')
<img src="{{ asset('images/logo-teks.svg') }}" class="logo" alt="Segment Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
