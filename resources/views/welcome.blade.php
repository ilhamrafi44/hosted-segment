@extends('layouts.apps')
@section('content')    
<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url('images/logo-teks.svg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="display-t">
                    <div class="display-tc animate-box" data-animate-effect="fadeIn">
                        <h1>Working hard is <strong>rewarding</strong></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="fh5co-project">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                <h2>Event Terkini</h2>
            </div>
        </div>
    </div>
    <div class="project-content">
        
        @foreach ($events as $item)
        <div class="col-forth">
            <div class="project animate-box" style="background-image:url('images/{{ $item->event_pict }}');">
                <a href="/transaksi/{{ $item->event_id }}" class="desc">
                    <h3 style="margin-bottom:1px;">{{ $item->event_name }}</h3>
                    <span>{{ $item->event_theme }}</span>
                </a>
            </div>
        </div>
        @endforeach
    </div>
    <div class="text-center">
        {!! $events->links() !!}
    </div>
</div>

<div id="fh5co-project">
    <div class="container">
        <div class="row animate-box" id="rank">
            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                <h2>Update Ranking Sementara.</h2>
            </div>
        </div>
    </div>
    <div class="project-content" >
        @php
         $no = 1;   
        @endphp
        @foreach ($users->sortByDesc('act_distance') as $items)
        <div class="col-forth">
            <div class="project animate-box" style="background-image:url('images/{{ $items->act_pict }}');">
                <a href="#" class="desc">
                    <h3 style="margin-bottom:1px;">{{ $items->name }} ({{ $items->act_distance }} Meter) - Urutan {{ $no++ }}</h3>
                    <span>{{ $items->act_captions }}</span>
                </a>
            </div>
        </div>
        @endforeach
    </div>
    <div class="text-center">
       
    </div>
</div>

@endsection