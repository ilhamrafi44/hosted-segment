<div>
<style>
    
.alert>.start-icon {
    margin-right: 0;
    min-width: 20px;
    text-align: center;
}

.alert>.start-icon {
    margin-right: 5px;
}

.greencross
{
  font-size:18px;
      color: #25ff0b;
    text-shadow: none;
}

.alert-simple.alert-success
{
  border: 1px solid rgba(36, 241, 6, 0.46);
    background-color: rgba(7, 149, 66, 0.12156862745098039);
    box-shadow: 0px 0px 2px #259c08;
    color: #0ad406;
  text-shadow: 2px 1px #00040a;
  transition:0.5s;
  cursor:pointer;
}
.alert-success:hover{
  background-color: rgba(7, 149, 66, 0.35);
  transition:0.5s;
}
.alert-simple.alert-info
{
  border: 1px solid rgba(6, 44, 241, 0.46);
    background-color: rgba(7, 73, 149, 0.12156862745098039);
    box-shadow: 0px 0px 2px #0396ff;
    color: #0396ff;
  text-shadow: 2px 1px #00040a;
  transition:0.5s;
  cursor:pointer;
}

.alert-info:hover
{
  background-color: rgba(7, 73, 149, 0.35);
  transition:0.5s;
}

.blue-cross
{
  font-size: 18px;
    color: #0bd2ff;
    text-shadow: none;
}

.alert-simple.alert-warning
{
      border: 1px solid rgba(241, 142, 6, 0.81);
    background-color: rgba(220, 128, 1, 0.16);
    box-shadow: 0px 0px 2px #ffb103;
    color: #ffb103;
    text-shadow: 2px 1px #00040a;
  transition:0.5s;
  cursor:pointer;
}

.alert-warning:hover{
  background-color: rgba(220, 128, 1, 0.33);
  transition:0.5s;
}

.warning
{
      font-size: 18px;
    color: #ffb40b;
    text-shadow: none;
}

.alert-simple.alert-danger
{
  border: 1px solid rgba(241, 6, 6, 0.81);
    background-color: rgba(220, 17, 1, 0.16);
    box-shadow: 0px 0px 2px #ff0303;
    color: #ff0303;
  transition:0.5s;
  cursor:pointer;
}

.alert-danger:hover
{
     background-color: rgba(220, 17, 1, 0.33);
  transition:0.5s;
}

.danger
{
      font-size: 18px;
    color: #ff0303;
    text-shadow: none;
}

.alert-simple.alert-primary
{
  border: 1px solid rgba(6, 241, 226, 0.81);
    background-color: rgba(1, 204, 220, 0.16);
    box-shadow: 0px 0px 2px #03fff5;
    color: #03d0ff;
    text-shadow: 2px 1px #00040a;
  transition:0.5s;
  cursor:pointer;
}

.alert-primary:hover{
  background-color: rgba(1, 204, 220, 0.33);
   transition:0.5s;
}

.alertprimary
{
      font-size: 18px;
    color: #03d0ff;
    text-shadow: none;
}

.square_box {
    position: absolute;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
    border-top-left-radius: 45px;
    opacity: 0.302;
}

.square_box.box_three {
    background-image: -moz-linear-gradient(-90deg, #290a59 0%, #3d57f4 100%);
    background-image: -webkit-linear-gradient(-90deg, #290a59 0%, #3d57f4 100%);
    background-image: -ms-linear-gradient(-90deg, #290a59 0%, #3d57f4 100%);
    opacity: 0.059;
    left: -80px;
    top: -60px;
    width: 500px;
    height: 500px;
    border-radius: 45px;
}

.square_box.box_four {
    background-image: -moz-linear-gradient(-90deg, #290a59 0%, #3d57f4 100%);
    background-image: -webkit-linear-gradient(-90deg, #290a59 0%, #3d57f4 100%);
    background-image: -ms-linear-gradient(-90deg, #290a59 0%, #3d57f4 100%);
    opacity: 0.059;
    left: 150px;
    top: -25px;
    width: 550px;
    height: 550px;
    border-radius: 45px;
}

.alert:before {
    content: '';
    position: absolute;
    width: 0;
    height: calc(100% - 44px);
    border-left: 1px solid;
    border-right: 2px solid;
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
    left: 0;
    top: 50%;
    transform: translate(0,-50%);
      height: 20px;
}

.fa-times
{
    -webkit-animation: blink-1 2s infinite both;
    animation: blink-1 2s infinite both;
}


/**
 * ----------------------------------------
 * animation blink-1
 * ----------------------------------------
 */
@-webkit-keyframes blink-1 {
  0%,
  50%,
  100% {
    opacity: 1;
  }
  25%,
  75% {
    opacity: 0;
  }
}
@keyframes blink-1 {
  0%,
  50%,
  100% {
    opacity: 1;
  }
  25%,
  75% {
    opacity: 0;
  }
}

</style>
    <div class="row">
          <!-- Registeration Form -->
          @foreach ($errors->all() as $error)
          <div class="col-md-12">
            <div class="alert fade alert-simple alert-danger alert-dismissible text-left font__family-montserrat font__size-16 font__weight-light brk-library-rendered rendered show" role="alert" data-brk-library="component__alert">
                <button type="button" class="close font__size-18" data-dismiss="alert">
                                          <span aria-hidden="true">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                          </span>
                                          <span class="sr-only">Close</span>
                                      </button>
                                      <i class="fa fa-times" aria-hidden="true"></i>
             {{ $error }}
              </div>    
          </div>
          @endforeach

        <!-- Email -->
        <div class="input-group col-lg-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white px-4 border-md border-right-0">
                    <i class="fa fa-user text-muted"></i>
                </span>
            </div>
            <input required type="text" name="email" id="email" placeholder="E-mail" class="form-control bg-white border-left-0 border-md">
        </div>


        <!-- Password -->
        <div class="input-group col-lg-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white px-4 border-md border-right-0">
                    <i class="fa fa-key text-muted"></i>
                </span>
            </div>
            <input required type="password" name="password" id="password" placeholder="Password" class="form-control bg-white border-left-0 border-md">
        </div>

      

        <div class="form-group col-lg-12">
            <button type="submit" class="btn btn-success btn-block py-3"><b><i class="fa fa-pencil-square" aria-hidden="true"></i> Login Sekarang</b></button>
        </div>

       

        {{-- <!-- Submit Button -->
        <div class="form-group col-lg-12 mx-auto mb-0">
            <a href="#" class="btn btn-primary btn-block py-2">
                <span class="font-weight-bold">Create your account</span>
            </a>
        </div> --}}

        <!-- Divider Text -->
        <div class="form-group col-lg-12 mx-auto d-flex align-items-center my-2">
            <div class="border-bottom w-100 ml-5"></div>
            <span class="px-2 small text-muted font-weight-bold text-muted">ATAU</span>
            <div class="border-bottom w-100 mr-5"></div>
        </div>

        <!-- Social Login -->
        <div class="form-group col-lg-12 mx-auto">
            <div class="row">
                <div class="col-md-6">
                    <a href="{{ url('auth/facebook') }}" class="btn btn-primary btn-block py-2 btn-facebook">
                        <i class="fa fa-facebook-f mr-2"></i>
                        <span class="font-weight-bold">Continue with Facebook</span>
                    </a>
                </div>
                <div class="col-md-6">
                    <a href="{{ url('auth/google') }}" class="btn btn-danger btn-block py-2 btn-danger">
                        <i class="fa fa-google mr-2"></i>
                        <span class="font-weight-bold">Continue with Google</span>
                    </a>
                </div>

            </div>
            
            
            

            
        </div>

        <!-- Already Registered -->
        <div class="text-center w-100">
            <p class="text-muted font-weight-bold">Pengguna Baru? <a href="/register" class="text-primary ml-2">Daftar Disini.</a></p>
        </div>

    </div>
   
</div>
