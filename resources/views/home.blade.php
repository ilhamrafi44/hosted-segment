@extends('layouts.app')

@section('content')

<div class="container">
  
    <div class="main-body">
     
    
          <!-- /Breadcrumb -->
    
          <div class="row gutters-sm">
            @if ($message = Session::get('success'))
            <div class="col-md-12 mb-3">
                 <div class="card-panel alert alert-success">{{ $message }}</div>
            </div>
           @endif
        
           @if ($message = Session::get('error'))
           <div class="col-md-12 mb-3">
             <div class="col-md-12 mb-3">
                 <div class="card-panel alert alert-danger">{{ $message }}</div>
                </div>
            </div>
           @endif
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                    @if (Auth::user()->user_pict == null)
                    <img src="{{ asset('images/user_default.png') }}" alt="User" class="rounded-circle" style="width:50%">
                        
                    @else
                    <img src="/images/{{ Auth::user()->user_pict }}" alt="User" class="rounded-circle" style="width:50%">
                        
                    @endif

                   
                  </div>
                </div>
              </div>
              <div class="card mt-3">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0">Status Pembayaran</h6>
                    @if ($users !== NULL)
                    @if ($payment !== NULL)
                    <span class="text-success">Sukses</span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <a class="btn btn-info text-white" href="/#rank">Klik Disini Untuk check rangking kamu</a>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0">Pesan Admin</h6>
                    <span class="text-info">{{ $users->trans_msg }}</span>
                  </li>

                    @else

                    <span class="text-danger">Belum Dikonfirmasi</span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0">Pesan Admin</h6>
                    <span class="text-info">{{ $users->trans_msg }}</span>
                  </li>
                    @endif
                    @else
                    <span class="text-success">Kamu Belum Melakukan Transaksi</span>
                  </li>
                    @endif
                  
                  
                  
                
                </ul>
              </div>
            </div>
            
            <div class="col-md-8">
         
              <div class="card mb-3">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Full Name</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      {{ Auth::user()->name }}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                        {{ Auth::user()->email }}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Phone</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                        {{ Auth::user()->no_hp }}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Address</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                        {{ Auth::user()->alamat }}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12 d-flex justify-content-between">
                      <a class="btn btn-info text-white" data-toggle="modal" data-target="#staticBackdrop">Edit Profile</a>
                      {{-- <a class="btn btn-info text-white" data-toggle="modal" data-target="#staticBackdrop1">Ubah Password</a> --}}
                      @if(!$users == FALSE)
                      <a class="btn btn-info text-white" href="/#fh5co-project">Ikut event lainnya.</a>
                      @endif
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row gutters-sm text-center">
              @if($users == FALSE)
                          <div class="col-md-12">
                              <h5>Anda Belum memilih event.</h5>
<a class="btn btn-info text-white" href="/#fh5co-project">Cari Event Disini.</a>
</div> 
     
              
              
              
              @else
                @if ($payment == NULL)
                <div class="col-md-12">
                <a class="btn btn-danger ml-3" href="https://api.whatsapp.com/send?phone=6281319228688&text=Halo%20Saya%20Ingin%20Bertanya%20Mengenai%20Konfirmasi%20Konfirmasi%20Pembayran%20Saya%20Dengan%20ID-INVOICE:%20{{ $users->u_trans }}." target="_blank">Hubungi Admin Jika dalam 2x24 jam pembayaran belum dikonrimasi.</a>
                </div> 
                @else
                @foreach($trans as $i)
                <div class="col-sm-6 mb-3">
                  <a href="/activities/{{ $i->event_id }}" style="text-decoration: none" class="card h-100">
                    <div class="card-body">
                      <h6 class="d-flex align-items-center mb-3"><i class="material-icons text-info mr-2">{{ $i->event_name }}</i>ID Transaksi : {{ $i->trans_id }}</h6>
                      <small>Total Pembayaran Kamu</small>
                      <div class="text-left">
                        <i class="material-icons text-info mr-2">Rp.{{ number_format($i->trans_total,0,",",".") }}</i>
                      </div>
                     <img src="/images/{{ $i->event_pict }}" alt="" style="width: 100%">
                    </div>
                  </a>
                </div>
                @endforeach
                @endif
            @endif

              </div>


            </div>
          </div>

        </div>
    </div>
    <!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Edit Profile Kamu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <form method="POST" action="/userUpdate/{{ Auth::user()->id }}" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="modal-body">
         <div class="row">
           
           <div class="col-md-6">
             <div class="form-group">
               <label for="">Nama kamu</label>
               <input value="{{ Auth::user()->name }}" type="text" class="form-control" name="name">
             </div>
           </div>

           <div class="col-md-6">
            <div class="form-group">
              <label for="">Email Kamu</label>
              <input disabled value="{{ Auth::user()->email }}" type="text" class="form-control">
              <small class="text-danger">*Hanya 1 email yang di izinkan setiap akun.</small>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="">Nomor Telfon</label>
              <input value="{{ Auth::user()->no_hp }}" type="text" class="form-control" name="no_hp">
            </div>
          </div>
         
          <div class="col-md-12">
            <div class="form-group">
              @if (Auth::user()->user_pict == NULL)
            <img src="{{ asset('images/user_default.png') }}" alt="User" class="rounded-circle" style="width:10%">
            @else
            <img src="/images/{{ Auth::user()->user_pict }}" alt="User" class="rounded-circle" style="width:10%">
            @endif
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label for="">Ubah Foto Profile</label>
              <input type="file" name="user_pict" class="form-control-file">
              <small class="text-danger">*Kosongkan Jika tidak ingin mengubah foto profil.</small>
            </div>
          </div>

         </div>
      </div>
      <div class="modal-footer">
        {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button> --}}
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>
    </form>
    </div>
  </div>
</div>
    <script>
      // Selecting all required elements
  
    </script>





<div class="modal fade" id="staticBackdrop1" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Ubah Password Kamu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <form method="POST" action="{{ route('updatepassword') }}" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="modal-body">
         <div class="row">
           <div class="col-md-6">
            <div class="form-group">
              <label for="">Password Lama</label>
              <input name="current_password" autocomplete="current_password" type="password" class="form-control">
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group">
              <label for="">Password Baru</label>
              <input name="new_password" autocomplete="new_password" type="password" class="form-control">
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label for="">Ulangi Password Baru</label>
              <input type="password" name="new_confirm_password" autocomplete="new_confirm_password" class="form-control">
              <small class="text-danger">*Kosongkan Jika tidak ingin mengubah password.</small>
            </div>
          </div>

         </div>
      </div>
      <div class="modal-footer">
        {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button> --}}
        <button type="submit" class="btn btn-primary">Ubah Password</button>
      </div>
    </form>
    </div>
  </div>
</div>
@endsection
