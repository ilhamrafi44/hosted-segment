
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Segment Events') }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Segment Sport Events Official Website" />
	<meta name="keywords" content="event sepeda, bokep indo, event, event olahraga, segment, segment sport, segment events, " />
	<meta name="author" content="segment.events" />
	<link href="{{ asset('images/logo-doang.svg') }}" rel="icon" type="image/svg">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	</head>
	<body>
@livewireStyles
<style>
    /*
*
* ==========================================
* CUSTOM UTIL CLASSES
* ==========================================
*

*/
.container {
    /* max-width: 840px;   */
}
.border-md {
    border-width: 2px;
}

.btn-facebook {
    background: #405D9D;
    border: none;
}

.btn-facebook:hover, .btn-facebook:focus {
    background: #314879;
}

.btn-twitter {
    background: #42AEEC;
    border: none;
}

.btn-twitter:hover, .btn-twitter:focus {
    background: #1799e4;
}



/*
*
* ==========================================
* FOR DEMO PURPOSES
* ==========================================
*
*/

body {
    min-height: 60vh;
}

.form-control:not(select) {
    padding: 1.5rem 0.5rem;
}

select.form-control {
    height: 52px;
    padding-left: 0.5rem;
    
}

select.form-control {
  padding-left: 20px;
}

.form-control::placeholder {
    color: #ccc;
    font-weight: bold;
    font-size: 0.9rem;
}
.form-control:focus {
    box-shadow: none;
}



</style>
<!-- Navbar-->


<div class="container" style="margin-top:200px;">
    <div class="row mt-3 align-items-center">
        <!-- For Demo Purpose -->
        <div class="col-md-3 pr-lg-5 ">
            <img src="{{ asset('images/logo-teks.svg') }}" alt="" class="img-fluid mb-1 d-none d-md-block">
            <h1 style="margin-bottom: 20px;">Login</h1>
            <small>Form</small>
           
        </div>

      
        <div class="col-md-9 col-lg-9 ml-auto">
            <form action="{{ route('login') }}" method="POST">
                @csrf
                @livewire('login')
            </form>
        </div>
    </div>
</div>
@livewireScripts

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>



 
</body>
</html>