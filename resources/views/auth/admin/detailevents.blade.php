
@extends('layouts.admins')

@section('admins')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
<script src="{{ asset('assets/js/plugins/jquery/dist/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <style>
.wrapper{
  position: absolute;
  top: 100px;
  left: 580px;
  animation: show_toast 1s ease forwards;
}
@keyframes show_toast {
  0%{
    transform: translateX(-100%);
  }
  40%{
    transform: translateX(10%);
  }
  80%, 100%{
    transform: translateX(20px);
  }
}
.wrapper.hide{
  animation: hide_toast 1s ease forwards;
}
@keyframes hide_toast {
  0%{
    transform: translateX(20px);
  }
  40%{
    transform: translateX(10%);
  }
  80%, 100%{
    opacity: 0;
    pointer-events: none;
    transform: translateX(-100%);
  }
}
.wrapper .toast{
  background: #fff;
  padding: 20px 15px 20px 20px;
  border-radius: 10px;
  border-left: 5px solid #2ecc71;
  box-shadow: 1px 7px 14px -5px rgba(0,0,0,0.15);
  width: 430px;
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.wrapper .toast.offline{
  border-color: #ccc;
}
.toast .content{
  display: flex;
  align-items: center;
}
.content .icon{
  font-size: 5px;
  color: #fff;
  height: 50px;
  width: 50px;
  text-align: center;
  line-height: 50px;
  border-radius: 50%;
  background: #2ecc71;
}
.toast.offline .content .icon{
  background: #ccc;
}
.content .details{
  margin-left: 15px;
}
.details span{
  font-size: 20px;
  font-weight: 500;
}
.details p{
  color: #878787;
}
.toast .close-icon{
  color: #878787;
  font-size: 23px;
  cursor: pointer;
  height: 40px;
  width: 40px;
  text-align: center;
  line-height: 40px;
  border-radius: 50%;
  background: #f2f2f2;
  transition: all 0.3s ease;
}
.close-icon:hover{
  background: #efefef;
}
    </style>
<!-- Header -->
<div class="header bg-dark pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
      <div class="header-body">
        <!-- Card stats -->
        @if ($message = Session::get('success'))
        <div class="wrapper">
        <div class="toast">
          <div class="content">
            <div class="icon"><i class="fa fa-check" style="font-size: 18px;"></i></div>
            <div class="details">
              <span>Cakep gan!</span>
              <p>{{ $message }}</p>
            </div>
          </div>
          <div class="close-icon"><i class="uil uil-times"></i></div>
        </div>
        </div>
        @endif
      </div>
    </div>
</div>
<div class="container-fluid mt--7">
      <!-- Dark table -->
      <div class="row mt-5">
        <div class="col">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-white mb-0">Edit Event {{ $events->event_name }}</h3>
            </div>
            <form action="/insertEditEvent/{{ $events->event_id }}" enctype="multipart/form-data" method="POST">
          @csrf
        @method('PUT')

        <div class="modal-body">
          <div class="row">
              <div class="col-md-6">
                <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-app"></i></span>
                      </div>
                      <input value="{{ $events->event_name }}" class="form-control" name="event_name" placeholder="Nama Event" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-album-2"></i></span>
                      </div>
                      <input value="{{ $events->event_theme }}" class="form-control" name="event_theme" placeholder="Tema Event" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Tanggal Mulai</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-album-2"></i></span>
                      </div>
                      <input id="datepicker7" class="form-control" name="event_start" placeholder="Tanggal Event" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Tanggal Selesai</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-album-2"></i></span>
                      </div>
                      <input id="datepicker8" class="form-control form-control-alternative" name="event_end" placeholder="Tanggal Event" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mb-3">
                    <label for="">Challenge</label>
                      <textarea class="ckeditor form-control form-control-alternative" name="event_challenge" id="" cols="30" rows="10">{{ $events->event_challenge }}</textarea>
                  </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mb-3">
                    <label for="">Destinasi</label>
                      <textarea class="ckeditor form-control form-control-alternative" name="event_destination" id="" cols="30" rows="10">{{ $events->event_destination }}</textarea>
                  </div>
              </div>

              <div class="col-md-12 mb-2">
                  <span>Metode Registrasi</span>
              </div>
              {{-- ULANG PAKE FOREACH DARI MODEL REGISGTRASI --}}
              @foreach ($regist as $item)
              <div class="col-md-12">
                <div class="form-group mb-3">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" value="{{ $item->regist_price }}" name="event_regist[]" class="custom-control-input" id="customCheck1" checked>
                        <label class="custom-control-label text-white" for="customCheck1">{{ $item->regist_name }} - Rp.{{ number_format($item->regist_price,0,",",".") }}</label>
                    </div>
                  </div>
              </div>
              @endforeach
              
             
              <hr>
              {{-- ULANG PAKE FOREACH DARI MODEL REGISGTRASI --}}

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Tanggal Terakhir Upload</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-album-2"></i></span>
                      </div>
                      <input id="datepicker9" class="form-control" name="event_lupload" placeholder="Tanggal Event" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Tanggal Pengiriman Baju</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-album-2"></i></span>
                      </div>
                      <input id="datepicker10" class="form-control" name="event_send" placeholder="Tanggal Event" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-user-run"></i></span>
                      </div>
                      <input value="{{ $events->event_target }}" class="form-control" name="event_target" placeholder="Jumlah Target" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mb-3">
                    <label for="">Metode Promosi</label>
                      <textarea class="ckeditor form-control form-control-alternative" name="event_promotion" id="" cols="30" rows="10">{{ $events->event_promotion }}</textarea>
                  </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mb-3">
                    <label for="">Media Distribusi Apparel</label>
                      <textarea class="ckeditor form-control form-control-alternative" name="event_dist_apparel" id="" cols="30" rows="10">{{ $events->event_dist_apparel }}</textarea>
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Logo sebelumnya</label>
                      <img width="100%" src="/images/{{ $events->event_logo }}" alt="">
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Gambar sebelumnya</label>
                    <img width="100%" src="/images/{{ $events->event_pict }}" alt="">
                  </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Logo event</label>
                      <input type="file" class="form-control form-control-alternative" name="event_logo">
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Gambar Event</label>
                      <input type="file" class="form-control form-control-alternative" name="event_pict">
                  </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mb-3">
                      <button class="btn btn-info btn-block"><i class="ni ni-send"></i> Submit</button>
                  </div>
              </div>
            </form>


      {{-- PAKE INI --}}
    </div>
        </div>
      </div>
 <script>
   // Selecting all required elements
const wrapper = document.querySelector(".wrapper"),
toast = wrapper.querySelector(".toast"),
title = toast.querySelector("span"),
subTitle = toast.querySelector("p"),
wifiIcon = toast.querySelector(".icon"),
closeIcon = toast.querySelector(".close-icon");

window.onload = ()=>{
    function ajax(){
        let xhr = new XMLHttpRequest(); //creating new XML object
        xhr.open("GET", "https://jsonplaceholder.typicode.com/posts", true); //sending get request on this URL
        xhr.onload = ()=>{ //once ajax loaded
            //if ajax status is equal to 200 or less than 300 that mean user is getting data from that provided url
            //or his/her response status is 200 that means he/she is online
            if(xhr.status == 200 && xhr.status < 300){
                closeIcon.onclick = ()=>{ //hide toast notification on close icon click
                    wrapper.classList.add("hide");
                }
                setTimeout(()=>{ //hide the toast notification automatically after 5 seconds
                    wrapper.classList.add("hide");
                }, 5000);
            }else{
                offline(); //calling offline function if ajax status is not equal to 200 or not less that 300
            }
        }
        xhr.onerror = ()=>{
            offline(); ////calling offline function if the passed url is not correct or returning 404 or other error
        }
        xhr.send(); //sending get request to the passed url
    }

    function offline(){ //function for offline
        wrapper.classList.remove("hide");
        toast.classList.add("offline");
        title.innerText = "You're offline now";
        subTitle.innerText = "Opps! Internet is disconnected.";
        wifiIcon.innerHTML = '<i class="uil uil-wifi-slash"></i>';
    }

    setInterval(()=>{ //this setInterval function call ajax frequently after 100ms
        ajax();
    }, 100);
}
 </script>
 <script>
   
var rupiah = document.getElementById('rupiah');
		rupiah.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			rupiah.value = formatRupiah(this.value, 'Rp. ');
		});
 
		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}
 </script>
 <script>
     
    $('#datepicker7').datepicker({
         weekStart: 1,
         daysOfWeekHighlighted: "6,0",
         autoclose: true,
         todayHighlight: true,
         format : 'yyyy/mm/dd'
     });
     $('#datepicker7').datepicker("setDate", new Date('{{ $events->event_start }}'));

     $('#datepicker8').datepicker({
         weekStart: 1,
         daysOfWeekHighlighted: "6,0",
         autoclose: true,
         todayHighlight: true,
         format : 'yyyy/mm/dd'
     });
     $('#datepicker8').datepicker("setDate", new Date('{{ $events->event_end }}'));

     $('#datepicker9').datepicker({
         weekStart: 1,
         daysOfWeekHighlighted: "6,0",
         autoclose: true,
         todayHighlight: true,
         format : 'yyyy/mm/dd'
     });
     $('#datepicker9').datepicker("setDate", new Date('{{ $events->event_lupload }}'));

     $('#datepicker10').datepicker({
         weekStart: 1,
         daysOfWeekHighlighted: "6,0",
         autoclose: true,
         todayHighlight: true,
         format : 'yyyy/mm/dd'
     });
     $('#datepicker10').datepicker("setDate", new Date('{{ $events->event_send }}'));
 </script>
@endsection