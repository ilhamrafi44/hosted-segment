
@extends('layouts.admins')

@section('admins')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
    <style>
.wrapper{
  position: absolute;
  top: 100px;
  left: 580px;
  animation: show_toast 1s ease forwards;
}
@keyframes show_toast {
  0%{
    transform: translateX(-100%);
  }
  40%{
    transform: translateX(10%);
  }
  80%, 100%{
    transform: translateX(20px);
  }
}
.wrapper.hide{
  animation: hide_toast 1s ease forwards;
}
@keyframes hide_toast {
  0%{
    transform: translateX(20px);
  }
  40%{
    transform: translateX(10%);
  }
  80%, 100%{
    opacity: 0;
    pointer-events: none;
    transform: translateX(-100%);
  }
}
.wrapper .toast{
  background: #fff;
  padding: 20px 15px 20px 20px;
  border-radius: 10px;
  border-left: 5px solid #2ecc71;
  box-shadow: 1px 7px 14px -5px rgba(0,0,0,0.15);
  width: 430px;
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.wrapper .toast.offline{
  border-color: #ccc;
}
.toast .content{
  display: flex;
  align-items: center;
}
.content .icon{
  font-size: 5px;
  color: #fff;
  height: 50px;
  width: 50px;
  text-align: center;
  line-height: 50px;
  border-radius: 50%;
  background: #2ecc71;
}
.toast.offline .content .icon{
  background: #ccc;
}
.content .details{
  margin-left: 15px;
}
.details span{
  font-size: 20px;
  font-weight: 500;
}
.details p{
  color: #878787;
}
.toast .close-icon{
  color: #878787;
  font-size: 23px;
  cursor: pointer;
  height: 40px;
  width: 40px;
  text-align: center;
  line-height: 40px;
  border-radius: 50%;
  background: #f2f2f2;
  transition: all 0.3s ease;
}
.close-icon:hover{
  background: #efefef;
}
    </style>
<!-- Header -->
<div class="header bg-dark pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
      <div class="header-body">
        <!-- Card stats -->
        @if ($message = Session::get('success'))
        <div class="wrapper">
        <div class="toast">
          <div class="content">
            <div class="icon"><i class="fa fa-check" style="font-size: 18px;"></i></div>
            <div class="details">
              <span>Mantap gan!</span>
              <p>{{ $message }}</p>
            </div>
          </div>
          <div class="close-icon"><i class="uil uil-times"></i></div>
        </div>
        </div>
        @endif
      </div>
    </div>
</div>
<div class="container-fluid mt--7">
    {{-- <button type="button" data-toggle="modal" data-target="#userModals" class="btn btn-neutral btn-icon text-sgmnt">
        <span class="btn-inner--icon"><i class="ni ni-user-run text-sgmnt" aria-hidden="true"></i></span>
        <span class="btn-inner--text">Tambah User</span>
    </button>

    <button type="button" data-toggle="modal" data-target="#registModal" class="btn btn-neutral btn-icon text-sgmnt">
        <span class="btn-inner--icon"><i class="ni ni-money-coins text-sgmnt" aria-hidden="true"></i></span>
        <span class="btn-inner--text">Tambah Registrasi</span>
    </button> --}}
      <!-- Dark table -->
      <div class="row mt-5">
        <div class="col">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-white mb-0">Daftar Aktivitas</h3>
            </div>
              <table class="table align-items-center table-dark table-flush" id="table-datatable">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">Urutan</th>
                    <th scope="col">Nama User</th>
                    <th scope="col">Total Jarak</th>
                    <th scope="col">Link Strava</th>
                    <th scope="col">Link Instagram</th>
                    <th scope="col">Screenshot Strava</th>
                    <th scope="col">Foto Kegiatan</th>
                    {{-- <th scope="col"></th> --}}
                  </tr>
                </thead>
                <tbody>
                  
                  @php
                      $no = 1;
                  @endphp
                  @foreach ($acts as $i)
                  <tr>
                    <th scope="row">{{ $no++ }}.</th>
                    <td>{{ $i->name }}</td>
                     <td>
                      @if ($i->act_distance == NULL)
                       <span class="badge badge-danger text-white badge-lg">Belum Upload</span>
                      @else
                      <span class="badge badge-success text-white badge-lg">Total Jarak {{ $i->act_distance }} Meter</span>
                      @endif
                    </td>
                    <td>
                      @if ($i->act_stravalink == NULL)
                       <span class="badge badge-danger text-white badge-lg">Belum Upload</span>
                      @else
                      <span class="badge badge-success text-white badge-lg">{{ $i->act_stravalink }}</span> 
                      @endif
                    </td>
                    <td>
                      @if ($i->act_instagramlink == NULL)
                       <span class="badge badge-danger text-white badge-lg">Belum Upload</span>
                      @else
                      <span class="badge badge-success text-white badge-lg">{{ $i->act_instagramlink }}</span>
                      @endif
                    </td>
                    <td>
                      @if ($i->act_pict == NULL)
                     <span class="badge badge-warning text-white badge-lg">Belum Upload</span>
                     @else
                     <div class="avatar-group">
                      <a href="#" data-toggle="modal" data-target="#pictModal{{ $i->act_id }}">
                        <img style="width:70%;" alt="Image placeholder" src="images/{{ $i->act_pict }}">
                      </a>
                    </div>
                     @endif
                    </td>
                    <td>
                    @if ($i->act_social_pict == NULL)
                     <span class="badge badge-warning text-white badge-lg">Belum Upload</span>
                     @else
                     <div class="avatar-group">
                      <a href="#" data-toggle="modal" data-target="#socialModal{{ $i->act_id }}">
                        <img style="width:70%;" alt="Image placeholder" src="images/{{ $i->act_social_pict }}">
                      </a>
                    </div>
                     @endif
                    </td>

    <div class="modal fade" id="pictModal{{ $i->act_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
              <img src="images/{{ $i->act_pict }}" alt="">
      </div>
    </div>
    </div>

    <div class="modal fade" id="socialModal{{ $i->act_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
                <img src="images/{{ $i->act_social_pict }}" alt="">
        </div>
      </div>
      </div>
                    @endforeach
                  </tr>
                </tbody>
              </table>


      {{-- PAKE INI --}}
    </div>

    <!-- Modal -->

      {{-- End Modal Here --}}

    </div>
  </div>

  <!-- Modal -->

  </div>
 <script>
   // Selecting all required elements
const wrapper = document.querySelector(".wrapper"),
toast = wrapper.querySelector(".toast"),
title = toast.querySelector("span"),
subTitle = toast.querySelector("p"),
wifiIcon = toast.querySelector(".icon"),
closeIcon = toast.querySelector(".close-icon");

window.onload = ()=>{
    function ajax(){
        let xhr = new XMLHttpRequest(); //creating new XML object
        xhr.open("GET", "https://jsonplaceholder.typicode.com/posts", true); //sending get request on this URL
        xhr.onload = ()=>{ //once ajax loaded
            //if ajax status is equal to 200 or less than 300 that mean user is getting data from that provided url
            //or his/her response status is 200 that means he/she is online
            if(xhr.status == 200 && xhr.status < 300){
                closeIcon.onclick = ()=>{ //hide toast notification on close icon click
                    wrapper.classList.add("hide");
                }
                setTimeout(()=>{ //hide the toast notification automatically after 5 seconds
                    wrapper.classList.add("hide");
                }, 5000);
            }else{
                offline(); //calling offline function if ajax status is not equal to 200 or not less that 300
            }
        }
        xhr.onerror = ()=>{
            offline(); ////calling offline function if the passed url is not correct or returning 404 or other error
        }
        xhr.send(); //sending get request to the passed url
    }

    function offline(){ //function for offline
        wrapper.classList.remove("hide");
        toast.classList.add("offline");
        title.innerText = "You're offline now";
        subTitle.innerText = "Opps! Internet is disconnected.";
        wifiIcon.innerHTML = '<i class="uil uil-wifi-slash"></i>';
    }

    setInterval(()=>{ //this setInterval function call ajax frequently after 100ms
        ajax();
    }, 100);
}
 </script>
 <script>
   
var rupiah = document.getElementById('rupiah');
		rupiah.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			rupiah.value = formatRupiah(this.value, 'Rp. ');
		});
 
		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}
 </script>

    


@endsection