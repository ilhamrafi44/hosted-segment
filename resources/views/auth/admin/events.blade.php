
@extends('layouts.admins')

@section('admins')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
    <style>
.wrapper{
  position: absolute;
  top: 100px;
  left: 580px;
  animation: show_toast 1s ease forwards;
}
@keyframes show_toast {
  0%{
    transform: translateX(-100%);
  }
  40%{
    transform: translateX(10%);
  }
  80%, 100%{
    transform: translateX(20px);
  }
}
.wrapper.hide{
  animation: hide_toast 1s ease forwards;
}
@keyframes hide_toast {
  0%{
    transform: translateX(20px);
  }
  40%{
    transform: translateX(10%);
  }
  80%, 100%{
    opacity: 0;
    pointer-events: none;
    transform: translateX(-100%);
  }
}
.wrapper .toast{
  background: #fff;
  padding: 20px 15px 20px 20px;
  border-radius: 10px;
  border-left: 5px solid #2ecc71;
  box-shadow: 1px 7px 14px -5px rgba(0,0,0,0.15);
  width: 430px;
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.wrapper .toast.offline{
  border-color: #ccc;
}
.toast .content{
  display: flex;
  align-items: center;
}
.content .icon{
  font-size: 5px;
  color: #fff;
  height: 50px;
  width: 50px;
  text-align: center;
  line-height: 50px;
  border-radius: 50%;
  background: #2ecc71;
}
.toast.offline .content .icon{
  background: #ccc;
}
.content .details{
  margin-left: 15px;
}
.details span{
  font-size: 20px;
  font-weight: 500;
}
.details p{
  color: #878787;
}
.toast .close-icon{
  color: #878787;
  font-size: 23px;
  cursor: pointer;
  height: 40px;
  width: 40px;
  text-align: center;
  line-height: 40px;
  border-radius: 50%;
  background: #f2f2f2;
  transition: all 0.3s ease;
}
.close-icon:hover{
  background: #efefef;
}
    </style>
<!-- Header -->
<div class="header bg-dark pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
      <div class="header-body">
        <!-- Card stats -->
        @if ($message = Session::get('success'))
        <div class="wrapper">
        <div class="toast">
          <div class="content">
            <div class="icon"><i class="fa fa-check" style="font-size: 18px;"></i></div>
            <div class="details">
              <span>Mantap gan!</span>
              <p>{{ $message }}</p>
            </div>
          </div>
          <div class="close-icon"><i class="uil uil-times"></i></div>
        </div>
        </div>
        @endif
      </div>
    </div>
</div>
<div class="container-fluid mt--7">
    <button type="button" data-toggle="modal" data-target="#eventsModal" class="btn btn-neutral btn-icon text-sgmnt">
        <span class="btn-inner--icon"><i class="ni ni-user-run text-sgmnt" aria-hidden="true"></i></span>
        <span class="btn-inner--text">Tambah Events</span>
    </button>

    <button type="button" data-toggle="modal" data-target="#registModal" class="btn btn-neutral btn-icon text-sgmnt">
        <span class="btn-inner--icon"><i class="ni ni-money-coins text-sgmnt" aria-hidden="true"></i></span>
        <span class="btn-inner--text">Tambah Registrasi</span>
    </button>
    <style>
    
      
    </style>
      <!-- Dark table -->
      <div class="row mt-5">
        <div class="col">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-white mb-0">Daftar Events</h3>
            </div>
              <table class="table align-items-center table-dark table-flush diplay nowrap" id="table-datatable">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">ID EVENT</th>
                    <th scope="col">Nama Events</th>
                    <th scope="col">Target</th>
                    <th scope="col">Waktu Event</th>
                    <th scope="col">Metode Registrasi</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  @php
                      $no = 1;
                  @endphp
                  @foreach ($events as $i)
                  <tr>
                    <th scope="row">{{ $no++ }}.</th>
                    <td>{{ $i->u_event }}</td>
                    <td>{{ $i->event_name }}</td>
                    <td><span class="badge badge-dot mr-4"><i class="bg-warning"></i> {{ $i->event_target }}</span></td>
                    <td>{{ $i->event_start }} - {{ $i->event_end }}</td>
                    <td>
                      @foreach ($i->event_regist as $u)
                      {{ $u }}
                      @endforeach
                    </td>
                    <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="/eventdetail/{{ $i->event_id }}">Edit</a>
                          <a class="dropdown-item" href="/eventDestroy/{{ $i->event_id }}">Hapus</a>
                        </div>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>


      {{-- PAKE INI --}}
    </div>

    <!-- Modal -->
<div class="modal fade" id="eventsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLabel"><i class="ni ni-user-run text-sgmnt" aria-hidden="true" style="font-size: 30px;"></i> Tambah Event</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('insertevents') }}" method="POST" enctype="multipart/form-data">
          @csrf
        <div class="modal-body">
          <div class="row">
              <div class="col-md-6">
                <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-app"></i></span>
                      </div>
                      <input class="form-control" name="event_name" placeholder="Nama Event" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-album-2"></i></span>
                      </div>
                      <input class="form-control" name="event_theme" placeholder="Tema Event" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Tanggal Mulai</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-album-2"></i></span>
                      </div>
                      <input id="datepicker" class="form-control" name="event_start" placeholder="Tanggal Event" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Tanggal Selesai</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-album-2"></i></span>
                      </div>
                      <input id="datepicker2" class="form-control form-control-alternative" name="event_end" placeholder="Tanggal Event" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mb-3">
                    <label for="">Challenge</label>
                      <textarea class="ckeditor form-control form-control-alternative" name="event_challenge" id="" cols="30" rows="10"></textarea>
                  </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mb-3">
                    <label for="">Destinasi</label>
                      <textarea class="ckeditor form-control form-control-alternative" name="event_destination" id="" cols="30" rows="10"></textarea>
                  </div>
              </div>

              <div class="col-md-12 mb-2">
                  <span>Metode Registrasi</span>
              </div>
              {{-- ULANG PAKE FOREACH DARI MODEL REGISGTRASI --}}
              @foreach ($regist as $item)
              <div class="col-md-12">
                <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-text">
                        <input type="checkbox" value="{{ $item->regist_price }}" name="event_regist[]" aria-label="Checkbox for following text input">
                      </div>
                      <input value="{{ $item->regist_name }} - Rp.{{ number_format($item->regist_price,0,",",".") }}" class="form-control form-control-alternative" readonly placeholder="Contoh Registrasi" type="text">
                    </div>
                  </div>
              </div>
              @endforeach
              
             
              <hr>
              {{-- ULANG PAKE FOREACH DARI MODEL REGISGTRASI --}}

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Tanggal Terakhir Upload</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-album-2"></i></span>
                      </div>
                      <input id="datepicker3" class="form-control" name="event_lupload" placeholder="Tanggal Event" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Tanggal Pengiriman Baju</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-album-2"></i></span>
                      </div>
                      <input id="datepicker4" class="form-control" name="event_send" placeholder="Tanggal Event" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-user-run"></i></span>
                      </div>
                      <input class="form-control" name="event_target" placeholder="Jumlah Target" type="text">
                    </div>
                  </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mb-3">
                    <label for="">Metode Promosi</label>
                      <textarea class="ckeditor form-control form-control-alternative" name="event_promotion" id="" cols="30" rows="10"></textarea>
                  </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mb-3">
                    <label for="">Media Distribusi Apparel</label>
                      <textarea class="ckeditor form-control form-control-alternative" name="event_dist_apparel" id="" cols="30" rows="10"></textarea>
                  </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Logo event</label>
                      <input type="file" class="form-control form-control-alternative" name="event_logo">
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="">Gambar Event</label>
                      <input type="file" class="form-control form-control-alternative" name="event_pict">
                  </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mb-3">
                      <button class="btn btn-info btn-block"><i class="ni ni-send"></i> Submit</button>
                  </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
<div class="modal fade" id="registModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Metode Registrasi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('insertregist') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <input type="text" class="form-control form-control-alternative" name="regist_name" id="" placeholder="Nama Registrasi">
                      </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <input type="text" id="rupiah" class="form-control form-control-alternative" name="regist_price" placeholder="Harga Registrasi">
                      </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="">Tanggal Mulai Registrasi</label>
                        <div class="input-group input-group-alternative">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-album-2"></i></span>
                          </div>
                          <input id="datepicker5" class="form-control" name="regist_start" placeholder="Tanggal Event" type="text">
                        </div>
                      </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="">Tanggal Selesai Registrasi</label>
                        <div class="input-group input-group-alternative">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-album-2"></i></span>
                          </div>
                          <input id="datepicker6" class="form-control" name="regist_end" placeholder="Tanggal Event" type="text">
                        </div>
                      </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group mb-3">
                      <button type="submit" class="btn btn-info"><i class="ni ni-send"></i> Tambah Metode Registrasi</button>
                    </div>
                  </div>
                </form>


            </div>
            
        </div>

      </div>
    </div>
  </div>
 <script>
   // Selecting all required elements
const wrapper = document.querySelector(".wrapper"),
toast = wrapper.querySelector(".toast"),
title = toast.querySelector("span"),
subTitle = toast.querySelector("p"),
wifiIcon = toast.querySelector(".icon"),
closeIcon = toast.querySelector(".close-icon");

window.onload = ()=>{
    function ajax(){
        let xhr = new XMLHttpRequest(); //creating new XML object
        xhr.open("GET", "https://jsonplaceholder.typicode.com/posts", true); //sending get request on this URL
        xhr.onload = ()=>{ //once ajax loaded
            //if ajax status is equal to 200 or less than 300 that mean user is getting data from that provided url
            //or his/her response status is 200 that means he/she is online
            if(xhr.status == 200 && xhr.status < 300){
                closeIcon.onclick = ()=>{ //hide toast notification on close icon click
                    wrapper.classList.add("hide");
                }
                setTimeout(()=>{ //hide the toast notification automatically after 5 seconds
                    wrapper.classList.add("hide");
                }, 5000);
            }else{
                offline(); //calling offline function if ajax status is not equal to 200 or not less that 300
            }
        }
        xhr.onerror = ()=>{
            offline(); ////calling offline function if the passed url is not correct or returning 404 or other error
        }
        xhr.send(); //sending get request to the passed url
    }

    function offline(){ //function for offline
        wrapper.classList.remove("hide");
        toast.classList.add("offline");
        title.innerText = "You're offline now";
        subTitle.innerText = "Opps! Internet is disconnected.";
        wifiIcon.innerHTML = '<i class="uil uil-wifi-slash"></i>';
    }

    setInterval(()=>{ //this setInterval function call ajax frequently after 100ms
        ajax();
    }, 100);
}
 </script>
 <script>
   
var rupiah = document.getElementById('rupiah');
		rupiah.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			rupiah.value = formatRupiah(this.value, 'Rp. ');
		});
 
		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}
 </script>
@endsection