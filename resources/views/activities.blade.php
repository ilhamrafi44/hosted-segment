@extends('layouts.app')

@section('content')
@foreach($events as $i)

<div class="container">
    <div class="main-body">
    <h3 class="text-center">Ayo Ikuti Event nya</h3>
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">

                    <img src="/images/{{ $i->event_logo }}" alt="Admin" width="100%">
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="card mb-3">
                <div class="card-body">
                 
                    @if ($acts == '')
                        <form method="POST" action="/insertAct" enctype="multipart/form-data">
                        @csrf
                    @else
                        <form method="POST" action="/updateAct/{{ $acts->act_id }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                    @endif
                    <input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="id_event" value="{{ $i->event_id }}">
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Total Jarak Kamu</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      @if ($acts == '')
                     <input required type="text" class="form-control" name="act_distance" placeholder="Misal : 100 M"> 
                          
                      @else
                     <input required type="text" class="form-control" name="act_distance" placeholder="Total Jarak Sebelumnya : {{ $acts->act_distance }}"> 
                      @endif
                    </div>
                  </div>
                  @if ($acts == '')
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                        <h6 class="mb-0">Link Strava Kamu</h6>
                      </div>
                      <div class="col-sm-9 text-secondary">
                       <input required type="text" class="form-control" name="act_stravalink" placeholder="Copy link dari aplikasi strava kamu kesini."> 
                      </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                        <h6 class="mb-0">Link Instagram Kamu</h6>
                      </div>
                      <div class="col-sm-9 text-secondary">
                       <input required type="text" class="form-control" name="act_instagramlink" placeholder="Copy link dari aplikasi instagram"> 
                      </div>
                  </div>
                  @endif
                  
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                        <h6 class="mb-0">Captions</h6>
                      </div>
                      <div class="col-sm-9 text-secondary">
                       <input required type="text" class="form-control" name="act_captions" placeholder="Misal : Asli sih ini keren banget."> 
                      </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                        <h6 class="mb-0">Upload Foto strava kamu</h6>
                      </div>
                      <div class="col-sm-9 text-secondary">
                       <input required type="file" class="form-control-file" name="act_pict" placeholder="Screenshot hasil strava kamu"> 
                      </div>
                  </div>
                  <hr>

                  <div class="row">
                    <div class="col-sm-3">
                        <h6 class="mb-0">Upload Foto narsis kamu juga</h6>
                      </div>
                      <div class="col-sm-9 text-secondary">
                       <input required type="file" class="form-control-file" name="act_social_pict" placeholder="Upload foto narsis mu disini."> 
                      </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-12 d-flex justify-content-end">
                      <button class="btn btn-info text-white" type="submit" >Upload Sekarang</button>
                    </div>
                  </div>
                </form>

                </div>
                
              </div>
              <div class="row gutters-sm">
                @if($acts == '')
                <div class="col-sm-6 mb-3">
                  <div class="card h-100">
                    <div class="card-body">
                      <span class="label label-info">Anda Belum Mengupload</span>
                    </div>
                </div>
                </div>
                @else
                <div class="col-sm-6 mb-3">
                  <div class="card h-100">
                    <div class="card-body">
                      <h6 class="d-flex align-items-center mb-3"><i class="material-icons text-info mr-2">{{ Auth::user()->name }}</i>Total Jarak Kamu : {{ $acts->act_distance }}</h6>
                      <small>{{ $acts->act_captions }}</small>
                     <img src="/images/{{ $acts->act_social_pict }}" alt="" style="width: 100%">
                    </div>
                </div>
                </div>
                @endif
              </div>

            </div>
          </div>

         

        </div>
        
    </div>
    
    @endforeach

    <script>

//    var km = 1613841.93424 / 1000;
// alert(km.toFixed(1) + " km");
    </script>
@endsection
