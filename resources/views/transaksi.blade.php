@extends('layouts.apps')
@section('content')    
<header id="fh5co-header" class="fh5co-cover" role="banner" style="height: 100px; background-image:url(images/img_bg_1.jpg);" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>

</header>
<style>
    #heading {
    text-transform: uppercase;
    color: #8cc725;
    font-weight: normal
}

#msform {
    position: relative;
    margin-top: 20px
}

#msform fieldset {
    background: transparent;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

.form-card {
    text-align: left
}

#msform fieldset:not(:first-of-type) {
    display: none
}

#msform input,
#msform textarea {
    padding: 8px 15px 8px 15px;
    border: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    background-color: #ECEFF1;
    font-size: 16px;
    letter-spacing: 1px
}

#msform input:focus,
#msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #8cc725;
    outline-width: 0
}

#msform .action-button {
    width: 100px;
    background: #8cc725;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right
}

#msform .action-button:hover,
#msform .action-button:focus {
    background-color: #311B92
}

#msform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px 10px 0px;
    float: right
}

#msform .action-button-previous:hover,
#msform .action-button-previous:focus {
    background-color: #000000
}

.card {
    z-index: 0;
    border: none;
    position: relative
}

.fs-title {
    font-size: 25px;
    color: #8cc725;
    margin-bottom: 15px;
    font-weight: normal;
    text-align: left
}

.purple-text {
    color: #8cc725;
    font-weight: normal
}

.steps {
    font-size: 25px;
    color: gray;
    margin-bottom: 10px;
    font-weight: normal;
    text-align: right
}

.fieldlabels {
    color: gray;
    text-align: left
}

#progressbar {
    margin-bottom: 30px;
    padding-left: 0px;
    overflow: hidden;
    color: lightgrey
}

#progressbar .active {
    color: #8cc725
}

#progressbar li {
    list-style-type: none;
    font-size: 15px;
    width: 33%;
    float: left;
    position: relative;
    font-weight: 400;
    text-align: center;
}

#progressbar #account:before {
    font-family: FontAwesome;
    content: "\f13e"
}

#progressbar #personal:before {
    font-family: FontAwesome;
    content: "\f007"
}

#progressbar #payment:before {
    font-family: FontAwesome;
    content: "\f030"
}

#progressbar #confirm:before {
    font-family: FontAwesome;
    content: "\f00c"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 20px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: #8cc725
}

.progress {
    height: 20px
}

.progress-bar {
    background-color: #8cc725
}

.fit-image {
    width: 100%;
    object-fit: cover
}

.radio-button {
  position: relative;
}
.radio-button input {
  position: absolute;
  margin: 5px;
  padding: 0;
  /* for mobile accessibility (iOS Label Bug) */
  visibility: hidden;
}

.radio-button .label-visible{
  margin-left: 2em;
  margin-bottom: 0;
}
.fake-radiobutton{
  position: absolute;
  display: block;
  top: 0;
  left: 3px;
  width: 20px;
  height: 20px;
  border: 1px solid slategray;
  background-color: white;
}
.fake-radiobutton:after {
  content: "";
  display: none;
  position: absolute;
  top: 50%;
  left: 50%;
  width: 16px;
  height: 16px;
  background: navy;
  transform: translateX(-50%) translateY(-50%);
}

.fake-radiobutton { border-radius: 50%; }
.fake-radiobutton:after { border-radius: 50%; }

input[type="radio"]:checked + span .fake-radiobutton:after { display: block; }

</style>


<div id="fh5co-started">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                    <div class="text-center">
                    <h2 class="text-sgmnt">Pembayaran Event {{ $events->event_name }}</h2>
                    <p>Mohon isi form dengan benar</p>
                </div>
                    <form id="msform" action="{{ route('inserttrans') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
                            <input type="hidden" name="id_event" value="{{ $events->event_id }}">
                        <!-- progressbar -->
                        <ul id="progressbar">
                            <li class="active" id="personal"><strong>Akun</strong></li>
                            <li id="account"><strong>Transaksi</strong></li>
                            <li id="confirm"><strong>Selesai</strong></li>
                        </ul>
                        <div class="progress" style="width: 100%;">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                        </div> <br> <!-- fieldsets -->
                        <fieldset>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Nama</label>
                                        <input readonly value="{{ Auth::user()->name }}" type="text" class="form-control" name="trans_name" id="" aria-describedby="helpId">
                                      </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Jumlah Transfer</label>
                                        <input readonly type="text" class="form-control" name="trans_total" id="trans_total" aria-describedby="helpId">
                                      </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                            @foreach ($regists as $m)
                                            <label class="radio-button">
                                                <input type="radio" name="trans_choice" value="Rp.{{ number_format($m->regist_price,0,",",".") }}">
                                                <span class="label-visible">
                                                  <span class="fake-radiobutton"></span>
                                                  Rp.{{ number_format($m->regist_price,0,",",".") }} - {{ $m->regist_name }} 
                                                </span>
                                              </label><br>
                                            @endforeach
                                        </div>
                                </div>
                            </div>
                           
                            <input type="button" name="next" class="next action-button" value="Next" />
                        </fieldset>

                        <fieldset>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Transfer Ke :</label><br>
                                        <span class="badge badge-warning">BCA : 3422604909</span><br>
                                        <span class="badge badge-warning">A/N : Nirmala Puri</span>
                                      </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Upload Bukti Transfer :</label><br>
                                        <input type="file" name="trans_prove" class="form-control">
                                    </div>
                                </div>
                            </div>
                             <button type="submit" name="next" class="next action-button" value="Next" />Bayar</button> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                        </fieldset>

                       

                        <fieldset>
                            <div class="form-card">
                                <div class="row">
                                </div> <br><br>
                                <h2 class="purple-text text-center"><strong>SUCCESS !</strong></h2> <br>
                                <div class="row justify-content-center">
                                    <div class="col-7 text-center">
                                        <h5 class="purple-text text-center">You Have Successfully Signed Up</h5>
                                    </div>
                                </div>
                            </div>
                            <input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                        </fieldset>
                        
                </div>
            </div>
        </form>
            
        </div>
    </div>
</div>
<script>
   
    var rupiah = document.getElementById('rupiah');
            rupiah.addEventListener('keyup', function(e){
                // tambahkan 'Rp.' pada saat form di ketik
                // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
                rupiah.value = formatRupiah(this.value, 'Rp. ');
            });
     
            /* Fungsi formatRupiah */
            function formatRupiah(angka, prefix){
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
     
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
     
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }
     </script>
@endsection