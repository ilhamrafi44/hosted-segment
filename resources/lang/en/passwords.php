<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Passwordmu telah di reset!',
    'sent' => 'Kami telah mengirim email untuk reset password.',
    'throttled' => 'Sedang memproses.',
    'token' => 'Token reset password salah.',
    'user' => "Tidak dapat menemukan user dengan email tersebut.",

];
