<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Provinsi;
use App\Models\Kota;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\PostalCode;
use DB;


class Register extends Component
{
    public $provinsi;
    public $kota;
    public $kecamatan;
    public $kelurahan;
    public $kode_pos;
    public $p1; //provinsi
    public $p2; //kota
    public $p3; //kecamatan
    public $p4; //kelurahan
    public $p5; //kodepos

    public function render()
    {
        $this->provinsi = Provinsi::all();

        $prov = $this->p1;
        $this->kota = Kota::where('prov_id', $prov)->get();

        $kot = $this->p2;
        $this->kecamatan = Kecamatan::where('city_id', $kot)->get();

        $kec = $this->p3;
        $this->kelurahan = Kelurahan::where('dis_id', $kec)->get();

        $kel = $this->p4;
        $this->kode_pos = DB::select("SELECT * FROM kode_pos WHERE subdis_id = '$kel' AND dis_id = '$kec' AND city_id = '$kot' AND prov_id = '$prov'");

        // $kec = $this->kecamatan;
        // $kel = $this->kelurahan;
        // $kod = $this->kode_pos;

       

        // $this->sekolah = DB::select("SELECT * FROM list_sekolah_sds WHERE (kel_sekolah LIKE '%$kelurahan%' AND kel_sekolah LIKE '%$rt%')");

        return view('livewire.register');
    }
}
