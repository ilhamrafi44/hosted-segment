<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Events;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    public function index(Request $request)
    {
        $events = Events::paginate(8);
        $users = Activity::join('users', 'activities.id_user', '=', 'users.id')
            ->orderBy('activities.act_distance', 'ASC')
            ->limit(8)->get();

        return view('welcome', compact('events', 'users'));
    }
}
