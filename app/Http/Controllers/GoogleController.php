<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Foundation\Auth\VerifiesEmails;

class GoogleController extends Controller
{
    use VerifiesEmails;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback()
    {
        try {

            $user = Socialite::driver('google')->user();

            $finduser = User::where('google_id', $user->id)->first();

            if ($finduser) {

                Auth::login($finduser);

                return redirect()->intended('Users');

            } else {
                 $config1 = [
            'table' => 'users',
            'field' => 'u_id',
            'length' => 8,
            'prefix' => 'USR-'
        ];
        
        // now use it
        $id = IdGenerator::generate($config1);
        
                $newUser = User::create([
                    'u_id' => $id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id' => $user->id,
                    'password' => encrypt('dumy123'),
                    'role' => 'user',
                ]);

                Auth::login($newUser);

                return redirect()->intended('Users');
            }

        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    public function facebookRedirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function loginWithFacebook()
    {
        try {

            $user = Socialite::driver('facebook')->user();
            $isUser = User::where('fb_id', $user->id)->first();

            if ($isUser) {
                Auth::login($isUser);
                return redirect('/Users');
            } else {
                   $config1 = [
            'table' => 'users',
            'field' => 'u_id',
            'length' => 8,
            'prefix' => 'USR-'
        ];
        
        // now use it
        $id = IdGenerator::generate($config1);
        
                $createUser = User::create([
                    'u_id' => $id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'fb_id' => $user->id,
                    'password' => encrypt('dumy123'),
                    'role' => 'user',

                ]);

                Auth::login($createUser);
                return redirect('/Users');
            }

        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
