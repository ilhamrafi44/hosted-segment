<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\EventRegist;
use App\Models\Events;
use App\Models\Transaction;
use App\Models\User;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware(['admin']);
    }

    public function index()
    {
        $events = Events::all()->count();
        $trans = Transaction::all()->count();
        $user = User::where('role', 'user')->count();
        $transp = Transaction::sum('trans_total');
        return view('auth.admin.index', compact('events', 'trans', 'user', 'transp'));
    }

    //Events START
    public function events()
    {
        $regist = EventRegist::all();
        $events = Events::all();

        return view('auth.admin.events', compact('regist', 'events'));
    }

    //Events START
    public function eventdetail($id)
    {
        $events = Events::where('event_id', $id)->first();
        $hitung = array_count_values($events->event_regist);
        $hasil = array_keys($hitung, max($hitung));
        $hasilpasti = implode("|", $hasil);
        $regist = EventRegist::where('regist_price', 'RLIKE', $hasilpasti)->get();

        return view('auth.admin.detailevents', compact('regist', 'events'));
    }

    public function insertEvents(Request $request)
    {
        $parah = strtoupper($request->event_name);
        $ganteng = str_replace(["A", "I", "U", "E", "O", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", " "], "", $parah);

        $config1 = [
            'table' => 'events',
            'field' => 'u_event',
            'length' => 10,
            'prefix' => "$ganteng-",
        ];

        // now use it
        $id = IdGenerator::generate($config1);

        $image = $request->file('event_logo');
        $image1 = $request->file('event_pict');
        $destinationPath1 = public_path("images");
        $profileImage1 = rand() . $request->input('event_name') . "." . $image1->extension();
        $image1->move($destinationPath1, $profileImage1);
        $input['event_pict'] = "$profileImage1";

        $destinationPath = public_path("images");
        $profileImage = rand() . $request->input('event_name') . "." . $image->extension();
        $image->move($destinationPath, $profileImage);
        $input['event_logo'] = "$profileImage";

        $events = Events::create([
            'u_event' => $id,
            'event_name' => $request->input('event_name'),
            'event_theme' => $request->input('event_theme'),
            'event_start' => $request->input('event_start'),
            'event_end' => $request->input('event_end'),
            'event_challenge' => $request->input('event_challenge'),
            'event_destination' => $request->input('event_destination'),
            'event_regist' => $request->input('event_regist'),
            'event_lupload' => $request->input('event_lupload'),
            'event_send' => $request->input('event_send'),
            'event_target' => $request->input('event_target'),
            'event_promotion' => $request->input('event_promotion'),
            'event_dist_apparel' => $request->input('event_dist_apparel'),
            'event_logo' => $profileImage,
            'event_pict' => $profileImage1,
        ]);

        return redirect('/events')->with(['success' => 'Event berhasil ditambahkan.']);
    }

    public function insertEditEvent(Request $request, $id)
    {
        //   dd($request->file('event_logo'));
        $old = Events::find($id);

        $input = $request->all();

        $events = new Events();
        if ($image = $request->file('event_logo') && $image1 = $request->file('event_pict')) {
            $destinationPath1 = public_path("images");
            $profileImage1 = date('YmdHis') . "." . $image1->extension();
            $image1->move($destinationPath1, $profileImage1);
            $input['event_pict'] = "$profileImage1";

            $destinationPath = public_path("images");
            $profileImage = date('YmdHis') . "." . $image->extension();
            $image->move($destinationPath, $profileImage);
            $input['event_logo'] = "$profileImage";

            $events->update([
                'event_name' => $input('event_name'),
                'event_theme' => $request->input('event_theme'),
                'event_start' => $request->input('event_start'),
                'event_end' => $request->input('event_end'),
                'event_challenge' => $request->input('event_challenge'),
                'event_destination' => $request->input('event_destination'),
                'event_regist' => $request->input('event_regist'),
                'event_lupload' => $request->input('event_lupload'),
                'event_send' => $request->input('event_send'),
                'event_target' => $request->input('event_target'),
                'event_promotion' => $request->input('event_promotion'),
                'event_dist_apparel' => $request->input('event_dist_apparel'),
                'event_logo' => $profileImage,
                'event_pict' => $profileImage1,
            ]);
        } elseif ($image = $request->file('event_logo')) {

            $image_path1 = public_path("images") . '/' . $old->event_logo;
            unlink($image_path1);
            $destinationPath = public_path("images");
            $profileImage = date('YmdHis') . "." . $image->extension();
            $image->move($destinationPath, $profileImage);
            $input['event_logo'] = "$profileImage";
            unset($input['event_pict']);
            $events->where('event_id', $id)->update([
                'event_name' => $input['event_name'],
                'event_theme' => $input['event_theme'],
                'event_start' => $input['event_start'],
                'event_end' => $input['event_end'],
                'event_challenge' => $input['event_challenge'],
                'event_destination' => $input['event_destination'],
                'event_regist' => $input['event_regist'],
                'event_lupload' => $input['event_lupload'],
                'event_send' => $input['event_send'],
                'event_target' => $input['event_target'],
                'event_promotion' => $input['event_promotion'],
                'event_dist_apparel' => $input['event_dist_apparel'],
                'event_logo' => $profileImage,
                // 'event_pict' => $event_picts,
            ]);

        } elseif ($image1 = $request->file('event_pict')) {

            $image_path = public_path("images") . '/' . $old->event_pict;
            unlink($image_path);
            $destinationPath1 = public_path("images");
            $profileImage1 = date('YmdHis') . "." . $image1->extension();
            $image1->move($destinationPath1, $profileImage1);
            $input['event_pict'] = "$profileImage1";
            unset($input['event_logo']);

            $events->where('event_id', $id)->update([
                'event_name' => $input['event_name'],
                'event_theme' => $input['event_theme'],
                'event_start' => $input['event_start'],
                'event_end' => $input['event_end'],
                'event_challenge' => $input['event_challenge'],
                'event_destination' => $input['event_destination'],
                'event_regist' => $input['event_regist'],
                'event_lupload' => $input['event_lupload'],
                'event_send' => $input['event_send'],
                'event_target' => $input['event_target'],
                'event_promotion' => $input['event_promotion'],
                'event_dist_apparel' => $input['event_dist_apparel'],
                // 'event_logo' => $event_logos,
                'event_pict' => $profileImage1,
            ]);

        } else {

            unset($input['event_logo']);
            unset($input['event_pict']);
            $events->where('event_id', $id)->update([
                'event_name' => $input['event_name'],
                'event_theme' => $input['event_theme'],
                'event_start' => $input['event_start'],
                'event_end' => $input['event_end'],
                'event_challenge' => $input['event_challenge'],
                'event_destination' => $input['event_destination'],
                'event_regist' => $input['event_regist'],
                'event_lupload' => $input['event_lupload'],
                'event_send' => $input['event_send'],
                'event_target' => $input['event_target'],
                'event_promotion' => $input['event_promotion'],
                'event_dist_apparel' => $input['event_dist_apparel'],
                // 'event_logo' => $event_logos,
                // 'event_pict' => $event_picts,
            ]);
        }

        return redirect('/events')->with(['success' => 'Event berhasil diedit.']);
    }

    public function insertRegist(Request $request)
    {
        $pay = str_replace(["Rp.", "."], "", $request->input('regist_price'));
        $events = EventRegist::create([
            'regist_name' => $request->input('regist_name'),
            'regist_price' => $pay,
            'regist_start' => $request->input('regist_start'),
            'regist_end' => $request->input('regist_end'),
        ]);

        return redirect('/events')->with(['success' => 'Metode registrasi berhasil diunggah.']);
    }
    //Events END

    //User START
    public function memberlist()
    {
        return view('auth.admin.member');
    }

    public function memberaktif()
    {
        $events = Events::all();
        $trans = Transaction::all();
        $users = Activity::join('transactions', 'activities.id_user', '=', 'transactions.id_user')->orderBy('activities.act_distance', 'DESC')
            ->get();

        $acts = User::join('transactions', 'users.id', '=', 'transactions.id_user')
            ->join('events', 'transactions.id_event', '=', 'events.event_id')->get();

        return view('auth.admin.memberaktif', compact('trans', 'users', 'acts', 'events'));
    }

    public function confirmpay(Request $request, $id)
    {
        $trans = Transaction::join('users', 'transactions.id_user', '=', 'users.id')->where('transactions.u_trans', $id)->first();
        $users = Transaction::where('u_trans', $id)->update([
            'trans_status' => 'approve',
            'trans_msg' => 'Pembayaran telah dikonfirmasi',
        ]);

        $ambonganteng = "Pembayaran kamu dengan nomor invoice : $trans->u_trans Telah kami konfirmasi ya. Ayo upload kegiatan mu di website kita : https://segment.events";

        $details = [
            'title' => 'Hai Sobat Segment',
            'body' => $ambonganteng,
        ];

        Mail::to("$trans->email")->send(new \App\Mail\PayMail($details));
        // Notification::send($trans, new OrderProcessed($users));
        // $request->all()->notify(new OrderProcessed($order));

        // $this->sendWhatsappNotification($id, $users->no_hp);
        return redirect('/memberaktif')->with(['success' => 'Berhasil Konfirmasi Pembayaran.']);
    }

    // private function sendWhatsappNotification(string $id, string $recipient)
    // {
    //     $twilio_whatsapp_number = "087723802268";
    //     $account_sid = "ACf90eb26c2878c9b67e899f4d9fe0593d";
    //     $auth_token = "bc4c25cfc79aacb4b244d909935cb6cb";

    //     $client = new Client($account_sid, $auth_token);
    //     $message = "Hai Sobat Segment. Pembayaran kamu dengan nomor invoice : {$id} Telah kami konfirmasi ya. Ayo upload kegiatan mu di website kita : https://segment.events $otp";
    //     return $client->messages->create("whatsapp:$recipient", array('from' => "whatsapp:$twilio_whatsapp_number", 'body' => $message));
    // }

    public function activitylist()
    {
        $events = Events::all();
        $acts = User::join('activities', 'users.id', '=', 'activities.id_user')->get();

        return view('auth.admin.activitylist', compact('acts', 'events'));
    }

    public function mem2berlist()
    {

    }
    //User END

    public function eventDestroy($event_id)
    {
        $car = Events::find($event_id);
        $image_path1 = public_path("images") . '/' . $car->event_logo;
        unlink($image_path1);
        $image_path = public_path("images") . '/' . $car->event_pict;
        unlink($image_path);

        $car->delete();

        return redirect('/events');

        // dd($id);
    }

    public function transDestroy($trans_id)
    {
        $car = Transaction::find($trans_id);
        $image_path1 = public_path("images") . '/' . $car->trans_prove;
        unlink($image_path1);

        $car->delete();

        return redirect('/memberaktif')->with(['success' => 'Berhasil Menghapus Data Transaksi.']);

        // dd($id);
    }
}
