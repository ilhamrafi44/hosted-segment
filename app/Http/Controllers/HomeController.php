<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Events;
use App\Models\Transaction;
use App\Models\User;
use DB;
// use App\Rules\MatchOldPassword;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['user', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id_user = Auth::user()->id;
        $trans = User::join('transactions', 'transactions.id_user', '=', 'users.id')
            ->join('events', 'events.event_id', '=', 'transactions.id_event')
            ->where('transactions.id_user', $id_user)
            ->get(['users.*', 'events.*', 'transactions.*']);

        $users = User::join('transactions', 'users.id', '=', 'transactions.id_user')
            ->where('transactions.id_user', $id_user)
            ->first();

        $payment = Transaction::where('id_user', $id_user)->where('trans_status', 'approve')->first();

        // dd($payment !== '');

        //    dd($users);

        return view('home', compact('trans', 'users', 'payment'));
    }

    public function transaksi($event_id)
    {
        $events = Events::find($event_id);
        $date1 = date("Y-m-d");
        $regists = DB::select("SELECT * FROM regist WHERE date(regist_end) >= '$date1' AND date(regist_end) <= '$events->event_end'");
        // dd($car);
        return view('transaksi', compact('events', 'regists'));
    }

    public function activities($event_id)
    {

        $events = Events::where('event_id', $event_id)->get();
        $acts = Activity::where('id_user', Auth::user()->id)->where('id_event', $event_id)->first();
        // dd($acts == '');
        return view('activities', compact('events', 'acts'));
    }

    public function insertTrans(Request $request)
    {
        $config1 = [
            'table' => 'transactions',
            'field' => 'u_trans',
            'length' => 8,
            'prefix' => 'INV-',
        ];

        // now use it
        $id = IdGenerator::generate($config1);

        $trans_proves = time() . '-' . $request->trans_name . '.' .
        $request->trans_prove->extension();
        $request->trans_prove->move(public_path('images'), $trans_proves);

        $pay = str_replace(["Rp.", "."], "", $request->input('trans_total'));
        $events = Transaction::create([
            'u_trans' => $id,
            'id_user' => $request->input('id_user'),
            'id_event' => $request->input('id_event'),
            'trans_name' => $request->input('trans_name'),
            'trans_total' => $pay,
            'trans_prove' => $trans_proves,
            'trans_message' => 'Pembayaran kamu sedang dikonfirmasi admin, mohon cek email mu secara berkala',
        ]);

        return redirect('/Users')->with(['success' => 'Terima Kasih telah melakukan pembayaran, Konfirmasi pembayaran akan dilakukan 1x24 jam. Mohon cek status anda secara berkala pada halaman profile']);

    }

    public function insertAct(Request $request)
    {
        $config1 = [
            'table' => 'activities',
            'field' => 'u_act',
            'length' => 8,
            'prefix' => 'ACT-',
        ];

        // now use it
        $id = IdGenerator::generate($config1);

        $image = $request->file('act_pict');
        $image1 = $request->file('act_social_pict');

        $destinationPath1 = public_path("images");
        $profileImage1 = rand() . "." . $image1->extension();
        $image1->move($destinationPath1, $profileImage1);
        $input['act_pict'] = "$profileImage1";

        $destinationPath = public_path("images");
        $profileImage = rand() . "." . $image->extension();
        $image->move($destinationPath, $profileImage);
        $input['act_social_pict'] = "$profileImage";

        $acts = strtolower($request->input('act_distance'));
        $pay = str_replace(["a", "i", "u", "e", "o", "km", "m", ",", ".", " "], "", $acts);
        $events = Activity::create([
            'u_act' => $id,
            'id_user' => $request->input('id_user'),
            'id_event' => $request->input('id_event'),
            'act_distance' => $pay,
            'act_stravalink' => $request->input('act_stravalink'),
            'act_instagramlink' => $request->input('act_instagramlink'),
            'act_captions' => $request->input('act_captions'),
            'act_pict' => $profileImage1,
            'act_social_pict' => $profileImage,
        ]);

        return redirect('/Users')->with(['success' => 'Keren!, Kamu telah mengikuti event. Semangsat gowes dan jangan lupa upload kesini lagi yaaaa']);

    }

    public function updateAct(Request $request, $id)
    {

        $old = Activity::find($id);

        $image_path = public_path("images") . '/' . $old->act_pict;
        unlink($image_path);

        $image_path1 = public_path("images") . '/' . $old->act_social_pict;
        unlink($image_path1);

        $image = $request->file('act_pict');
        $image1 = $request->file('act_social_pict');

        $destinationPath1 = public_path("images");
        $profileImage1 = rand() . "." . $image1->extension();
        $image1->move($destinationPath1, $profileImage1);
        $input['act_pict'] = "$profileImage1";

        $destinationPath = public_path("images");
        $profileImage = rand() . "." . $image->extension();
        $image->move($destinationPath, $profileImage);
        $input['act_social_pict'] = "$profileImage";

        $acts = strtolower($request->input('act_distance'));
        $pay1 = str_replace(["a", "i", "u", "e", "o", "km", "m", ",", ".", " "], "", $acts);
        
        $events = Activity::where('act_id', $id)->update([
            'id_user' => $request->input('id_user'),
            'id_event' => $request->input('id_event'),
            'act_distance' => $pay1,
            'act_captions' => $request->input('act_captions'),
            'act_pict' => $profileImage1,
            'act_social_pict' => $profileImage,
        ]);

        return redirect('/Users')->with(['success' => 'Keren!, Kamu baru saja update proggress mu. Semangsat gowes dan jangan lupa upload kesini lagi yaaaa']);

    }

    public function userUpdate(Request $request, $id)
    {

        $old = User::find($id);
        $input = $request->all();

        $users = new User();

        if ($old->user_pict == null && $image = $request->file('user_pict')) {
            $destinationPath = public_path("images");
            $profileImage = rand() . Auth::user()->name . "." . $image->extension();
            $image->move($destinationPath, $profileImage);
            $input['user_pict'] = "$profileImage";

            $users = User::where('id', $id)->update([
                'user_pict' => $profileImage,
                'name' => $request->input('name'),
                'no_hp' => $request->input('no_hp'),
            ]);
        } elseif ($image1 = $request->file('user_pict')) {

            $image_path1 = public_path("images") . '/' . $old->user_pict;
            unlink($image_path1);
            $destinationPath = public_path("images");
            $profileImage1 = rand() . "." . $image1->extension();
            $image1->move($destinationPath, $profileImage1);
            $input['user_pict'] = "$profileImage1";

            $users = User::where('id', $id)->update([
                'user_pict' => $profileImage1,
                'name' => $request->input('name'),
                'no_hp' => $request->input('no_hp'),
            ]);
        } else {
            $users = User::where('id', $id)->update([
                'name' => $request->input('name'),
                'no_hp' => $request->input('no_hp'),
            ]);
        }

        return redirect('/Users')->with(['success' => 'Asik!, Profile Kamu berhasil diupdate.']);

    }

    // public function updatePassword(Request $request){
    //     $request->validate([
    //         'current_password' => ['required', new MatchOldPassword],
    //         'new_password' => ['required'],
    //         'new_confirm_password' => ['same:new_password'],
    //     ]);

    //     User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

    //     dd('Password change successfully.');
    //       return back()->with('success', 'Password Berhasil Diganti.');
    // }
}