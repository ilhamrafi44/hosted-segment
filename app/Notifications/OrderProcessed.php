<!-- // app/Notifications/OrderProcessed.php -->

<?php

namespace App\Notifications;

use App\Channels\Messages\WhatsAppMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Channels\WhatsAppChannel;
use App\Transaction;


class OrderProcessed extends Notification
{
  use Queueable;


  public $users;
  
  public function __construct(Transaction $users)
  {
    $this->transaction = $user;
  }
  
  public function via($notifiable)
  {
    return [WhatsAppChannel::class];
  }
  
  public function toWhatsApp($notifiable)
  {
    $orderUrl = $this->transaction->u_trans;
    $deliveryDate = $this->transaction->created_at->addDays(4)->toFormattedDateString();


    return (new WhatsAppMessage)
        ->content("Hai Sobat Segment. Pembayaran kamu dengan nomor invoice : {$orderUrl} Pada tanggal {$deliveryDate} Telah kami konfirmasi ya. Ayo upload kegiatan mu di website kita : https://segment.events");
  }
}