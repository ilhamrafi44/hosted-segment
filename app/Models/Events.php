<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    use HasFactory;

    protected $table = 'events';
    protected $primaryKey = 'event_id';

    protected $fillable = 
    ['u_event',
    'event_name', 
    'event_theme', 
    'event_start', 
    'event_end',
    'event_challenge',
    'event_destination',
    'event_regist',
    'event_lupload',
    'event_send',
    'event_target',
    'event_promotion',
    'event_dist_apparel',
    'event_logo',
    'event_pict'];

    public function setEventRegistAttribute($value)
    {
        $this->attributes['event_regist'] = json_encode($value);
    }

    public function getEventRegistAttribute($value)
    {
        return $this->attributes['event_regist'] = json_decode($value);
    }

}
