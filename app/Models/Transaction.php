<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'transactions';
    protected $primaryKey = 'trans_id';

    protected $fillable = 
    ['u_trans',
    'id_user', 
    'id_event', 
    'trans_name', 
    'trans_total',
    'trans_prove',
    'trans_status',
    'trans_msg'];
}
