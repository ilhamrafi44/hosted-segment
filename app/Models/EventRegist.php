<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventRegist extends Model
{
    use HasFactory;

    protected $table = 'regist';
    protected $primaryKey = 'regist_id';

    protected $fillable = 
    ['regist_name', 
    'regist_price', 
    'regist_start', 
    'regist_end'];
}
