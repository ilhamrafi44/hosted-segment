<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $table = 'activities';
    protected $primaryKey = 'act_id';

    protected $fillable = 
    ['u_act',
    'id_user', 
    'id_event', 
    'act_distance', 
    'act_stravalink',
    'act_instagramlink',
    'act_captions',
    'act_pict',
    'act_social_pict'];
}
