<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    use HasFactory;

    protected $table = 'kelurahan';
    protected $primaryKey = 'subdis_id';

    protected $fillable = ['subdis_name', 'dis_id'];

    // protected $timestamps = true;

    // protected $dateFormat = 'h:m:s';
}
