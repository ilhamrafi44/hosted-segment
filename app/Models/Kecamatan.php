<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    use HasFactory;

    protected $table = 'kecamatan';
    protected $primaryKey = 'dis_id';

    protected $fillable = ['dis_name', 'city_id'];

    // protected $timestamps = true;

    // protected $dateFormat = 'h:m:s';
}
