<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    use HasFactory;

    protected $table = 'provinsi';
    protected $primaryKey = 'prov_id';

    protected $fillable = ['prov_name'];

    // protected $timestamps = true;

    // protected $dateFormat = 'h:m:s';
}
