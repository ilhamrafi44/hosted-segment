<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('event_id');
            $table->string('u_event')->unique()->nullable();
            $table->string('event_name')->nullable();
            $table->string('event_theme')->nullable();
            $table->date('event_start')->nullable();
            $table->date('event_end')->nullable();
            $table->longText('event_challenge')->nullable();
            $table->longText('event_destination')->nullable();
            $table->string('event_regist')->nullable();
            $table->date('event_lupload')->nullable();
            $table->date('event_send')->nullable();
            $table->string('event_target')->nullable();
            $table->longText('event_promotion')->nullable();
            $table->longText('event_dist_apparel')->nullable();
            $table->int('event_target_distance')->nullable();
            $table->string('event_logo')->nullable();
            $table->string('event_pict')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
