<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('trans_id');
            $table->string('u_trans')->unique()->nullable();
            $table->string('id_user')->nullable();
            $table->string('id_event')->nullable();
            $table->string('trans_name')->nullable();
            $table->string('trans_total')->nullable();
            $table->string('trans_prove')->nullable();
            $table->string('trans_status')->nullable();
            $table->string('trans_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
