<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('act_id');
            $table->string('u_act')->unique()->nullable();
            $table->string('id_user')->nullable();
            $table->string('id_event')->nullable();
            $table->string('act_distance')->nullable();
            $table->string('act_stravalink')->nullable();
            $table->string('act_instagramlink')->nullable();
            $table->string('act_captions')->nullable();
            $table->string('act_pict')->nullable();
            $table->string('act_social_pict')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
